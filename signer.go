package main

import (
	"sort"
	"strconv"
	"strings"
	"sync"
)

func main() {

}

func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	in := make(chan interface{})
	for _, job := range jobs {
		wg.Add(1)
		out := make(chan interface{})
		go startworker(job, in, out, wg)
		in = out
	}
	wg.Wait()
}

func startworker(job job, in, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()
	defer close(out)
	job(in, out)
}

func crc32signer(data string, out chan string) {
	out <- DataSignerCrc32(data)
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}

	for v := range in {
		wg.Add(1)
		go singleHashCalc(v.(int), out, wg, mu)
	}
	wg.Wait()
}

func singleHashCalc(value int, out chan interface{}, wg *sync.WaitGroup, mu *sync.Mutex) {
	defer wg.Done()
	data := strconv.Itoa(value)

	mu.Lock()
	md5 := DataSignerMd5(data)
	mu.Unlock()

	crc32Chan := make(chan string)
	crc32md5Chan := make(chan string)

	go crc32signer(data, crc32Chan)
	go crc32signer(md5, crc32md5Chan)

	crc32 := <-crc32Chan
	crc32md5 := <-crc32md5Chan

	out <- crc32 + "~" + crc32md5
}

func MultiHash(in, out chan interface{}) {
	th := 6
	wg := &sync.WaitGroup{}
	for v := range in {
		wg.Add(1)
		go multiHashCalc(v.(string), out, wg, th)
	}
	wg.Wait()
}

func multiHashCalc(value string, out chan interface{}, wg *sync.WaitGroup, ths int) {
	defer wg.Done()
	results := make([]string, ths)
	wgCalc := &sync.WaitGroup{}
	muCalc := &sync.Mutex{}
	for th := 0; th < ths; th++ {
		wgCalc.Add(1)
		data := strconv.Itoa(th) + value

		go func(data string, th int, results []string, wgCalc *sync.WaitGroup, muCalc *sync.Mutex) {
			result := DataSignerCrc32(data)
			muCalc.Lock()
			results[th] = result
			muCalc.Unlock()
			wgCalc.Done()
		}(data, th, results, wgCalc, muCalc)
	}
	wgCalc.Wait()
	out <- strings.Join(results, "")
}

func CombineResults(in, out chan interface{}) {
	resultsSlice := make([]string, 0)
	for v := range in {
		resultsSlice = append(resultsSlice, v.(string))
	}
	sort.Strings(resultsSlice)

	out <- strings.Join(resultsSlice, "_")
}
